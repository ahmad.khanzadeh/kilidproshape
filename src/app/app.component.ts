import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'kilidProShape';
  event:any;
     theWidth=window.innerWidth;

     @HostListener('window:resize', ['$event'])
    onResize() {
         this.theWidth = window.innerWidth;
    }
  
}
