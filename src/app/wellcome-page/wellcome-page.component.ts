import { Component, OnInit,HostListener } from '@angular/core';

@Component({
  selector: 'app-wellcome-page',
  templateUrl: './wellcome-page.component.html',
  styleUrls: ['./wellcome-page.component.scss']
})
export class WellcomePageComponent implements OnInit {
theWidth:number=0;
  constructor() { }

  ngOnInit(): void {
    this.theWidth=window.innerWidth;
  }
  @HostListener('window:resize', ['$event'])
  onResize() {
      this.theWidth = window.innerWidth;
      }
  
}
