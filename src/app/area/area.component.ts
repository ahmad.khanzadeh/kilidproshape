
import { Component, OnInit,HostListener , OnChanges, SimpleChanges } from '@angular/core';
import {FormGroup, FormControl,Validators} from '@angular/forms'

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss']
})
export class AreaComponent implements OnInit {

  form=new FormGroup({
    theCity: new FormControl('',Validators.required),
    theTown: new FormControl('', Validators.required)
  });

  selectedCity=[
    {id:1, city:"تهران"},
    {id:2, city:"مشهد"},
    {id:3, city:"اصفهان"},
    {id:4, city:"کرج"},
    {id:5, city:"شیراز"}
  ]

   selectedTown=[
     {id:1, area:"منطقه ۱"},
     {id:2, area:"منطقه۲"},
     {id:3, area:"منطقه ۳"},
     {id:4, area:"منطقه ۴"},
     {id:5, area:"منطقه ۵"}
   ]
  

  areaInputs:any=[];
  totalArea:number=this.selectedTown.length;
  customerSelect:number=0;
  
  
  theWidth:number=0;
  constructor() { }

  ngOnInit(): void {
    this.theWidth=window.innerWidth;
    console.log(this.theWidth);
  }

  @HostListener('window:resize', ['$event'])
    onResize() {
      this.theWidth = window.innerWidth;
      
    }

  onSubmit(){
      
      if(this.areaInputs.indexOf(this.form.value.theTown) ==-1){
        this.areaInputs.push(this.form.value.theTown);
        this.customerSelect+=1;
        console.log(this.totalArea);
        console.log(this.customerSelect);
       
      }
      
    }

  ngOnChanges(topic: HTMLSelectElement){
    if(this.form.value.theTown==""){
      console.log("warning: Empty Option is selected- wont show in selected area!")
      console.log(this.form.value.theTown);
      topic.value='';
    }else{
      if(this.areaInputs.length==0){
         console.log('Array was Empty');
         this.areaInputs.push(this.form.value.theTown);
         this.customerSelect+=1;
         topic.value='';
       }else{
         if(this.areaInputs.indexOf(this.form.value.theTown) ==-1){
           this.areaInputs.push(this.form.value.theTown);
           this.customerSelect+=1;
           topic.value='';
             }
             topic.value='';
       }
    } 
    
  }
}
