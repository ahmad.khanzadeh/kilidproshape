import { Component, OnInit,HostListener } from '@angular/core';

@Component({
  selector: 'app-main-body',
  templateUrl: './main-body.component.html',
  styleUrls: ['./main-body.component.scss']
})
export class MainBodyComponent implements OnInit {
  theWidth:number=0;
  constructor() { }

  ngOnInit(): void {
   
    this.theWidth=window.innerWidth;
  }
  @HostListener('window:resize', ['$event'])
   onResize() {
       this.theWidth = window.innerWidth;
       
  }

}
