import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { MainBodyComponent } from './main-body/main-body.component';
import { ReactiveFormsModule } from '@angular/forms';
import { WellcomePageComponent } from './wellcome-page/wellcome-page.component';
import { AreaComponent } from './area/area.component';
import { MobileHeaderComponent } from './mobile-header/mobile-header.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    SidebarComponent,
    MainBodyComponent,
    WellcomePageComponent,
    AreaComponent,
    MobileHeaderComponent 
  ],
  
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path:'', component:MainBodyComponent},
      {path:'home',component:MainBodyComponent},
      {path:'area',component:AreaComponent},
      {path:'welcome',component:WellcomePageComponent},
      {path:'**', component:MainBodyComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
