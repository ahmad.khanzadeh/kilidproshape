import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  theWidth:number=0;
  constructor() { }

  ngOnInit(): void {
    this.theWidth=window.innerWidth;
    console.log(this.theWidth);
  }

}
